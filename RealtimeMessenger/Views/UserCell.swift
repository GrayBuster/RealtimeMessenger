//
//  UserCell.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/31/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class UserCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var detailedLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var unseenMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.addCircle(withBorderWidth: 1, .black)
        unseenMessageLabel?.layer.backgroundColor  = UIColor.black.cgColor
        unseenMessageLabel?.layer.masksToBounds = true
        unseenMessageLabel?.layer.cornerRadius = 20
    }
    
    var channel: Channel? {
        didSet {
            if let channel = channel , let partnerId = channel.partnerId() {
                if channel.uids.count > 2 {
                    let ref = Database.database().reference().child("Users")
                    var name = ""
                    ref.observe(.childAdded, with: { snapshot in
                        if let value = snapshot.value as? [String:Any] {
                            let user = User(from: value, snapshot.key)
                            if channel.uids.contains(user.uid) && !user.uid.contains(Auth.auth().currentUser!.uid) {
                                if name.isEmpty {
                                    name = user.username
                                }else{
                                    name = name + "," + user.username
                                }
                            }
                            DispatchQueue.main.async {
                                channel.name = name
                                self.titleLabel?.text = name
                                let message = Message(from: channel.messages.last!)
                                if !message.text.isEmpty {
                                    self.detailedLabel.text = message.text
                                } else {
                                    let ref = Database.database().reference().child("Users").child(message.fromId)
                                    ref.observeSingleEvent(of: .value, with: { (snapshot) in
                                        if let value = snapshot.value as? [String:Any] {
                                            let user = User(from: value, snapshot.key)
                                            DispatchQueue.main.async {
                                                self.detailedLabel.text = user.username + " has sent you an image"
                                            }
                                        }
                                    })
                                }
                                
                                if user.image != "" {
                                    self.profileImageView.downloaded(from: user.image, completion: nil)
                                }else {
                                    self.profileImageView.image = #imageLiteral(resourceName: "user profile")
                                }
                                self.timeLabel.text = message.convertTimeToString()
                                if !message.isSeen && Auth.auth().currentUser?.uid != message.fromId {
                                    self.unseenMessageLabel.isHidden = false
                                    self.unseenMessageLabel.text = ""
                                } else {
                                    self.unseenMessageLabel.isHidden = true
                                }
                            }
                        }
                        
                    }, withCancel: nil)
                } else {
                    Database.database().reference().child("Users").child(partnerId).observeSingleEvent(of: .value, with: { snapshot in
                        if let value = snapshot.value as? [String:Any] {
                            let user = User(from: value, snapshot.key)
                            DispatchQueue.main.async {
                                self.titleLabel?.text = user.username
                                let message = Message(from: channel.messages.last!)
                                if !message.text.isEmpty {
                                    self.detailedLabel.text = message.text
                                } else {
                                    let ref = Database.database().reference().child("Users").child(message.fromId)
                                    ref.observeSingleEvent(of: .value, with: { (snapshot) in
                                        if let value = snapshot.value as? [String:Any] {
                                            let user = User(from: value, snapshot.key)
                                            DispatchQueue.main.async {
                                                if message.fromId != Auth.auth().currentUser?.uid {
                                                    self.detailedLabel.text = user.username + " has sent you an image"
                                                } else {
                                                    self.detailedLabel.text = "You has sent an image"
                                                }
                                            }
                                        }
                                    })
                                }
                                if user.image != "" {
                                    self.profileImageView.downloaded(from: user.image, completion: nil)
                                } else {
                                    self.profileImageView.image = #imageLiteral(resourceName: "user profile")
                                }
                                self.timeLabel.text = message.convertTimeToString()
                                if !message.isSeen && Auth.auth().currentUser?.uid != message.fromId {
                                    self.unseenMessageLabel.isHidden = false
                                    self.unseenMessageLabel.text = ""
                                } else {
                                    self.unseenMessageLabel.isHidden = true
                                }
                            }
                        }else {
                            print("nil")
                        }
                    })
                }
            }
        }
    }
    
    var message: Message? {
        didSet {
            setupUsernameAndImageProfile()
        }
    }
    
    private func setupUsernameAndImageProfile() {
        if let message = message, let id = message.chatPartnerId {
            
            Database.database().reference().child("Users").child(id).observeSingleEvent(of: .value, with: { snap in
                if let value = snap.value as? [String:Any] {
                    let user = User(from: value, message.fromId)
                    DispatchQueue.main.async {
                        self.titleLabel?.text = user.username
                        self.detailedLabel.text = message.text
                        if user.image != "" {
                            self.profileImageView.downloaded(from: user.image, completion: nil)
                        }else {
                            self.profileImageView.image = #imageLiteral(resourceName: "user profile")
                        }
                        self.timeLabel.text = message.convertTimeToString()
                        if Auth.auth().currentUser?.uid != message.fromId {
//                            self.unseenMessageLabel.isHidden = false
//                            self.unseenMessageLabel.text = "\(self.countUnseen(message: message))"
                        } else {
//                            self.unseenMessageLabel.isHidden = true
                        }
                        
                        
                    }
                }else {
                    print("nil")
                }
            })
        }
    }
    
    var user: User? {
        didSet {
            if let user = user {
                titleLabel.text = user.username
                detailedLabel.text = user.email
                if user.image != "" {
                    profileImageView.downloaded(from: user.image, completion: nil)
                }else {
                    self.profileImageView.image = #imageLiteral(resourceName: "user profile")
                }
                
            }
        }
    }
    
}
