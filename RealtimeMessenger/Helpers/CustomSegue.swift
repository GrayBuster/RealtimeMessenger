//
//  SegueFromLeft.swift
//  SieuThiNhaDat
//
//  Created by gray buster on 8/10/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit

class CustomLeftSegue: UIStoryboardSegue {
    override func perform() {
        let src = self.source
        let dst = self.destination
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: -src.view.frame.size.width, y: 0)
        
        UIView.animate(withDuration: 0.25,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
        },
                       completion: { finished in
                        
                        src.present(dst, animated: false, completion: nil)
                        
                        
        })
    }
}

class CustomUnwindSegue: UIStoryboardSegue {
    override func perform() {
        let toViewController: UIViewController = self.destination
        let fromViewController: UIViewController = self.source

        let screenBounds: CGRect = UIScreen.main.bounds

        let finalToFrame: CGRect = screenBounds
        let finalFromFrame: CGRect = finalToFrame.offsetBy(dx: screenBounds.size.width, dy: 0)

        toViewController.view.frame = finalToFrame.offsetBy(dx: -screenBounds.size.width, dy: 0)
        UIView.animate(withDuration: 0.5, animations: {

            toViewController.view.frame = finalFromFrame
            fromViewController.view.frame = finalToFrame

        }, completion: { finished in
            let fromVC: UIViewController = self.source
            let _: UIViewController = self.destination
            fromVC.dismiss(animated: false, completion: nil)
        })
    }
}
