//
//  ChatLogViewController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/30/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import NotificationCenter

protocol HandleDismissDelegate {
    func handleDismissKeyboard()
}

class InboxController: UICollectionViewController,UITextViewDelegate,UICollectionViewDelegateFlowLayout {
    

    var channel: Channel?
    
    var receiver: User?
    
    var delegate: HandleDismissDelegate?
    
    var delegatePassingChannel: PassingChannelDelegate?
    
    var isBecomeResponder = true
    
    var containerView: UIView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 8, right: 0)
        collectionView?.alwaysBounceVertical = true
        collectionView?.keyboardDismissMode = .interactive
        
        observeChannel()
    }
    
    override var inputAccessoryView: UIView? {
        return containerView
    }
    
    override var canBecomeFirstResponder: Bool {
        return isBecomeResponder
    }
    
    
    func observeMessages() {
        guard let channel = channel else { print("channel is nil"); return }
        let channelRef = Database.database().reference().child("Channel").child(channel.id)
        channelRef.observe(.childChanged, with: { (snapshot) in
            if let value = snapshot.value as? [[String:Any]] {
                self.channel!.messages.removeAll()
                self.channel!.messages = value
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    if self.channel!.messages.count > 0 {
                        let indexpath = IndexPath(item: channel.messages.count - 1, section: 0)
                        self.collectionView?.scrollToItem(at: indexpath, at: .bottom, animated: true)
                    }
                }
            }
            
        }, withCancel: nil)
    }
    
    //Observe Channel else oberver messages
    func observeChannel() {
        guard let receiver = receiver,let uid = Auth.auth().currentUser?.uid else { print("nil"); return }
        if channel == nil {
            let channelRef = Database.database().reference().child("Channel")
            channelRef.observe(.childAdded, with: { snapshot in
                if let value = snapshot.value as? [String:Any] {
                    let channel = Channel(from: value)
                    if channel.uids.count <= 2 {
                        if channel.uids.contains(receiver.uid) && channel.uids.contains(uid) {
                            DispatchQueue.main.async {
                                self.channel = channel
                                self.observeMessages()
                                self.collectionView?.reloadData()
                                if self.channel!.messages.count > 0 {
                                    let indexpath = IndexPath(item: channel.messages.count - 1, section: 0)
                                    self.collectionView?.scrollToItem(at: indexpath, at: .bottom, animated: true)
                                }
                            }
                        }
                    }
                }
            }, withCancel: nil)
        }else {
            self.observeMessages()
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let channel = channel else { return 0 }
        return channel.messages.count
    }
  
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Inbox Cell", for: indexPath) as! InboxCell
        if let channel = channel {
            let message = channel.toMessages[indexPath.row]
            cell.textView.text = message.text
            setup(cell, with: message)
            if !message.text.isEmpty {
                cell.bubbleWidthAnchor?.constant = estimatedFrame(for: channel.toMessages[indexPath.row].text).width + 32
            }
            
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.handleDismissKeyboard()
        guard let channel = channel else { return }
        if channel.messages.count > 0 {
            let indexpath = IndexPath(item: channel.messages.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: indexpath, at: .bottom, animated: true)
        }
        
    }

    //Configure cell with messaage
    private func setup(_ cell: InboxCell,with message: Message) {
        
        if !message.text.isEmpty {
            
        }
        if message.fromId == Auth.auth().currentUser?.uid {
            //Sender Message
            cell.bubbleView.backgroundColor = InboxCell.blueColor
            cell.textView.textColor = .white
            cell.profileImageView.isHidden = true
            cell.bubbleLeftAnchor?.isActive = false
            cell.bubbleRightAnchor?.isActive = true
        }else {
            //Reciever Message
            cell.bubbleView.backgroundColor = UIColor(r: 240, g: 240, b: 240)
            cell.textView.textColor = .black
            cell.bubbleLeftAnchor?.isActive = true
            cell.bubbleRightAnchor?.isActive = false
            cell.profileImageView.isHidden = false
            if let channel = channel {
                
                //Channel has more than 3 people
                if channel.uids.count > 2 {
                    Database.database().reference().child("Users").child(message.toId).observe(.value, with: { snapshot in
                        if let value = snapshot.value as? [String:Any] {
                            let user = User(from: value, snapshot.key)
                            if !user.image.isEmpty {
                                cell.profileImageView.downloaded(from: user.image, completion: nil)
                            } else {
                                cell.profileImageView.image = #imageLiteral(resourceName: "user profile")
                            }
                            
                        }
                    }, withCancel: nil)
                } else {
                    //Channel has only 2 people
                    Database.database().reference().child("Users").child(message.toId).observeSingleEvent(of: .value, with: { snapshot in
                        if let value = snapshot.value as? [String:Any] {
                            let user = User(from: value, snapshot.key)
                            if user.image != "" {
                                cell.profileImageView.downloaded(from: user.image, completion: nil)
                            } else {
                                cell.profileImageView.image = #imageLiteral(resourceName: "user profile")
                            }
                        }
                    }, withCancel: nil)
                }
            }
        }
        if let imageUrl = message.imageUrl {
            cell.messageImageView.downloaded(from: imageUrl, completion: nil)
            cell.messageImageView.isHidden = false
            cell.bubbleView.backgroundColor = .clear
        } else {
            cell.messageImageView.isHidden = true
        }
    }
    
    //Layout text
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        let width = UIScreen.main.bounds.width
        if let channel = channel {
            if !channel.toMessages[indexPath.item].text.isEmpty {
                height = estimatedFrame(for: channel.toMessages[indexPath.item].text).height + 20
                let width = UIScreen.main.bounds.width
                return CGSize(width: width, height: height)
            }
        }
        return CGSize(width: width, height: height)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }

    //Calculated frame message's text
    private func estimatedFrame(for text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text)
            .boundingRect(with: size,
                          options: options,
                          attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)],
                          context: nil)
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
