//
//  ViewController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 8/28/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseUI
import FirebaseDatabase
import FirebaseStorage


class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var iconImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var iconImageView: UIImageView!{
        didSet {
            iconImageView.addCircle(withBorderWidth: 1, .black)
        }
    }
    
    
    
    @IBOutlet var loadingView: UIView!
    
    
    var ref: DatabaseReference! {
        return Database.database().reference()
    }
    
    var imagePicker = UIImagePickerController()
    
    var user: User?
    
    var register = false
    
    
    
    
    @IBAction func switchFunction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            actionBtn.setTitle("Login", for: .normal)
            usernameTextField.isHidden = true
            iconImageView.isUserInteractionEnabled = false
            iconImageView.image = #imageLiteral(resourceName: "talk icon")
            register = !register
        case 1:
            actionBtn.setTitle("Register", for: .normal)
            usernameTextField.isHidden = false
            iconImageView.isUserInteractionEnabled = true
            iconImageView.image = #imageLiteral(resourceName: "user profile")
            register = !register
        default:
            break
        }
    }
    
    @IBAction func onClickedBtn(_ sender: UIButton) {
        guard let email = emailTextField.text , let password = passwordTextField.text , let username = usernameTextField.text else { return }
        if register {
            handleRegister(with: email, password, username, iconImageView.image!)
        } else {
            handleLogin(with: email, password)
        }
    }
    
    
    func handleRegister(with email: String,_ password: String,_ username: String,_ image: UIImage) {
        if !email.isEmpty && !password.isEmpty && !username.isEmpty {
            self.view.addSubview(loadingView)
            //Register user
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if error != nil {
                    DispatchQueue.main.async {
                        self.loadingView.removeFromSuperview()
                    }
                    print(error!)
                    return
                }
                guard authResult != nil , let uid = authResult?.user.uid else {
                    DispatchQueue.main.async {
                        self.loadingView.removeFromSuperview()
                    }
                    return
                }
                
                //Store image to firebase storage
                let storageRef = Storage.storage().reference().child("user_profileimages").child("\(uid).jpg")
                
                if let uploadData = UIImageJPEGRepresentation(image, 0.1) {
                    storageRef.putData(uploadData, metadata: nil, completion: { metaData, error in
                        if error != nil {
                            print(error!)
                            return
                        }
                        storageRef.downloadURL(completion: { url, error in
                            if error != nil {
                                print(error!)
                                return
                            }
                            //Save User to database
                            if let url = url {
                                self.user = User(uid: uid,email: email, password: password, username: username,image: url.absoluteString)
                                self.ref.child("Users").child(uid).setValue(self.user!.toAny())
                                Auth.auth().signIn(withEmail: self.user!.email, password: self.user!.password, completion: nil)
                                DispatchQueue.main.async {
                                    self.loadingView.removeFromSuperview()
                                    self.perform(#selector(self.handlePresentingAlreadyLoginController), with: nil, afterDelay: 0)
                                }
                            }else {
                                self.user = User(uid: uid,email: email, password: password, username: username,image: "")
                                self.ref.child("Users").child(uid).setValue(self.user!.toAny())
                                Auth.auth().signIn(withEmail: self.user!.email, password: self.user!.password, completion: nil)
                                DispatchQueue.main.async {
                                    self.loadingView.removeFromSuperview()
                                    self.perform(#selector(self.handlePresentingAlreadyLoginController), with: nil, afterDelay: 0)
                                }
                            }
                        })
                    })
                }
            }
        }
    }
    
    func handleLogin(with email: String,_ password: String) {
        if !email.isEmpty && !password.isEmpty{
            self.view.addSubview(loadingView)
            Auth.auth().signIn(withEmail: email, password: password) { authResult,error in
                if error != nil {
                    if let errorCode = AuthErrorCode(rawValue: error!.code) {
                        DispatchQueue.main.async {
                            self.loadingView.removeFromSuperview()
                            self.handleErrors(with: errorCode)
                            return
                        }
                    }
                }
                if authResult != nil {
                    Database.database().reference().child("Users").child(authResult!.user.uid).observeSingleEvent(of: .value, with: { snapshot in
                        if let value = snapshot.value as? [String:Any] {
                            let key = snapshot.key
                            self.user = User(from: value,key)
                            DispatchQueue.main.async {
                                self.loadingView.removeFromSuperview()
                                self.perform(#selector(self.handlePresentingAlreadyLoginController), with: nil, afterDelay: 1.0)
                            }
                        }else {
                            print("nil")
                        }
                        
                    }, withCancel: nil)
                }
            }
        }
    }
    
    //Handle Login Errors
    private func handleErrors(with code: AuthErrorCode) {
        switch code {
        case .emailAlreadyInUse:
            let alert = UIAlertController(title: "Oops!", message:"Sorry,Email taken", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true,completion: nil)
        case .invalidEmail:
            let alert = UIAlertController(title: "Oops!", message:"Invalid email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true,completion: nil)
        case .networkError:
            let alert = UIAlertController(title: "Oops!", message:"Network error, check your connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true)
        case .wrongPassword:
            let alert = UIAlertController(title: "Oops!", message:"Wrong password!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true)
        default:
            print("Unknown Error")
            let alert = UIAlertController(title: "Oops!", message:"Unknown Error", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func handleImageTapped(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //Taking photo by camera
    func openCamera() {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))  {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Taking photo by gallary
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func handlePresentingAlreadyLoginController() {
        self.performSegue(withIdentifier: "Login Successful", sender: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.center = view.center
        loadingView.layer.cornerRadius = 25
        imagePicker.delegate = self
        usernameTextField.text = nil
        emailTextField.text = nil
        passwordTextField.text = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Login Successful" {
            if let messagesVC = segue.destination.content as? MessagesController, let user = self.user {
                messagesVC.user = user
            }else {
                print("nil")
            }
        }
    }
}


//MARK: Image Picker Delegate
extension LoginViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            iconImageView.image = chosenImage
        }else {
            iconImageView.image = #imageLiteral(resourceName: "user profile")
        }
        picker.dismiss(animated: true, completion: nil)
    }
}







