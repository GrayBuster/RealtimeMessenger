//
//  UsersTableController.swift
//  RealtimeMessenger
//
//  Created by gray buster on 9/6/18.
//  Copyright © 2018 gray buster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

protocol AddingUserDelegate {
    func didAdd(_ user: User)
}

class UsersTableController: UITableViewController {
    
    var users = [User]()
    
    var channel: Channel?
    
    var receiver: User?
    
    var uids = [String]()
    
    var delegate: AddingUserDelegate?
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.center = view.center
        view.addSubview(spinner)
        fetchUsers()
    }
    
    func fetchUsers() {
        tableView.separatorStyle = .none
        spinner.startAnimating()
        Auth.auth().addStateDidChangeListener { authResult, authUser in
            if authUser != nil {
                Database.database().reference().child("Users").observe(.childAdded, with: { snapshot in
                    if let value = snapshot.value as? [String:Any] {
                        let key = snapshot.key
                        let user = User(from: value,key)
                        
                        if let channel = self.channel {
                            if !channel.uids.contains(user.uid) {
                                self.users.append(user)
                            }
                        }else if let receiver = self.receiver {
                            if !authUser!.uid.contains(user.uid) && !receiver.uid.contains(user.uid) {
                                self.users.append(user)
                            }
                        } else {
                            if !authUser!.uid.contains(user.uid) {
                                self.users.append(user)
                            }
                        }
                        DispatchQueue.main.async {
                            self.spinner.stopAnimating()
                            self.tableView.separatorStyle = .singleLine
                            self.tableView.reloadData()
                        }
                        
                    }else {
                        print("nil")
                    }
                }, withCancel: nil)
            }
        }
    }


    @IBAction func cancelAddingUser(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = self.users[indexPath.row]
        self.delegate?.didAdd(user)
        users.remove(at: indexPath.row)
        dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersCell", for: indexPath) as! UserCell
        
        let user = users[indexPath.row]
        cell.user = user
        

        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
